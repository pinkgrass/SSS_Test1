from setuptools import setup, find_packages

setup(name='strategy',
      version='0.1',
      description='example strategy',
      url='http://pinkgrass.github.io/Spring-Street-Securities',
      author='Richard Crook',
      author_email='richard@pinkgrass.org',
      license='Apache',
      packages=find_packages(exclude=['contrib','docs','tests*']),
      install_requires=['pyalgotrade']
      )