# PyAlgoTrade - Download Market Data 
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse

import pyalgotrade.logger
from pyalgotrade import bar

from ripple import barfeed # generalise the barfeed
from ripple import ripplecharts # generalise for market data download

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("storagepath", help="path to store market data", type=str, default='data/')
    parser.add_argument("frequency", help="bar frequency in seconds", type=str, default='86400')
    parser.add_argument('days', help='number of days of data to download', type=str, default='90')
    args = parser.parse_args()
    storagepath = args.storagepath
    frequency = int(args.frequency)
    days = int(args.days)
    return storagepath,frequency,days

if __name__ == '__main__':
    # python marketdata.py marketdata BTC.E2q_XRP
    #
    # parse arguments, find top markets, download data for each market
    #
    logger = pyalgotrade.logger.getLogger("market data")
    logger.info("Starting")

    # Parse arguments - storage,[markets,frequency,days], --refresh
    #storage = 'data/'
    # [86400, 3600, 60]
    #frequencies = [bar.Frequency.DAY, bar.Frequency.HOUR, bar.Frequency.MINUTE]
    #days = 90
    storage,frequency,days = parse_arguments()

    frequencies =[frequency]
    
    logger.info('Market data will be saved to %s' % storage)
    logger.info('Frequencies to be downloaded %s' % frequencies)
    logger.info('Number of days to download %d' % days)
    
    markets = ripplecharts.top_markets().keys()
    logger.info('Ripple markets found %s' % str(len(markets)))
    
    for frequency in frequencies:
        periods = ((24*60*60) / frequency) * 90 # 90 days of data
        logger.info('Downloading market data for frequency,period: %s,%s' % (frequency,periods))
        ripplecharts.build_feed_recent(markets, periods=periods, storage=storage, frequency=frequency)

    logger.info('Finishing')
