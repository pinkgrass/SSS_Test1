# Unit Tests

from glob import glob
import logging
import inspect
import imp
import itertools
import multiprocessing

from pyalgotrade.stratanalyzer import returns
from pyalgotrade.stratanalyzer import sharpe
from pyalgotrade.stratanalyzer import trades
import pyalgotrade.logger

from ripple import barfeed

log = logging.getLogger('BackTester')
log.setLevel(logging.INFO)
format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# move helper functions to module
def generate_parameters(strategy):
    logging.debug("Generating parameters")
    parameters = [['instrument']] + strategy.parameters()
    parameter_cnt = reduce(lambda x, y: x * y, [len(x) for x in parameters])
    logging.debug("Found %d parameters" % (parameter_cnt))
    return itertools.product(*parameters)

def load_strategy(strategy_path):
    logging.debug("Loading strategy source code from %s" % (strategy_path))
    strategy_module = imp.load_source('BacktestingStrategy',strategy_path)
    strategy_name, strategy_class = inspect.getmembers(strategy_module)[0] #loads first class found
    logging.debug("Found strategy %s" % (strategy_name))
    return strategy_class

def load_market_data(marketdatafile):
    logging.debug("Loading market data from %s" % (marketdatafile))
    feed = barfeed.Feed()  
    feed.addBarsFromCSV('instrument', marketdatafile)
    return feed

def test_strategies():
    strategy_files = glob('./strategy/*.py')
    strategy_files.remove('./strategy/__init__.py')
    logging.info('Found %d strategies' % len(strategy_files))

    market_files = glob('./data/*.csv')
    logging.info('Found %d markets' % len(market_files))

    for market_file in market_files[1:2]:
        feed = load_market_data(market_file)
        for strategy_file in strategy_files:
            strategy = load_strategy(strategy_file) 
            for parameter in generate_parameters(strategy):
                yield run_strategy, strategy, parameter, feed

def run_strategy(strategy,parameter,feed):
    _multiprocess_can_split_ = True
    #print(strategy,parameter,feed)
    myStrategy = strategy(feed,*parameter)

    # Attach analyzers to strategy
    retAnalyzer = returns.Returns()
    sharpeRatioAnalyzer = sharpe.SharpeRatio()
    tradesAnalyzer = trades.Trades()

    myStrategy.attachAnalyzer(retAnalyzer)
    myStrategy.attachAnalyzer(sharpeRatioAnalyzer)
    myStrategy.attachAnalyzer(tradesAnalyzer)

    myStrategy.run()

    # Strategy Risk = Probability x Impact
    # = ( Profitable trades / total trades ) x Returns

    strategy_return = ((float(myStrategy.getResult())-1000000.0) / 1000000.0) * 100.0
    if tradesAnalyzer.getCount() == 0:
        profitable_ratio = 0
    else:
        profitable_ratio = (float(tradesAnalyzer.getProfitableCount()) / float(tradesAnalyzer.getCount()))
    riskscore = profitable_ratio * strategy_return

    results = {
        #'sharpe':       sharpeRatioAnalyzer.getSharpeRatio(0.05),
        'return':       strategy_return,
        'count':        tradesAnalyzer.getCount(),
        'profit_count': tradesAnalyzer.getProfitableCount(),
        'profitable_ratio': profitable_ratio,
        'riskscore': riskscore
        }
    # add strategy file, market data file

    #logging.info("Result: %s" % str(results))
    if results['return'] > 0:
        logging.info("Result: %s" % str(results))

    return results

if __name__ == '__main__':
    logging.info('Starting backtest')
    strategy_files = glob('./strategy/*.py')
    strategy_files.remove('./strategy/__init__.py')
    logging.info('Found %d strategies' % len(strategy_files))

    market_files = glob('./data/*.csv')
    logging.info('Found %d markets' % len(market_files))

    cores = multiprocessing.cpu_count()
    logging.info('Found %d processing cores' % cores)
    pool = multiprocessing.Pool(processes=cores)
  
    results = []

    strategy = load_strategy(strategy_files[0])    
    for market_file in market_files:
        feed = load_market_data(market_file)
        for parameter in generate_parameters(strategy):
            # This will cause context switching, better to use Pool
            #multiprocessing.Process(target=run_strategy, args=(strategy,parameter,feed)).start()
            try: 
                results += [run_strategy(strategy,parameter,feed)]
            except Exception as e:
                #logging.error('Strategy backtest failed: %s with arguments %s' % (e.message,e.args))
                pass

    logging.info('Ran %d tests' % len(results))    

    logging.info('Finishing backtest')