# PyAlgoTrade - Strategy Performance Analysis
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import inspect
import imp
import argparse

from pyalgotrade.stratanalyzer import returns
from pyalgotrade.stratanalyzer import sharpe
from pyalgotrade.stratanalyzer import drawdown
from pyalgotrade.stratanalyzer import trades
import pyalgotrade.logger

from ripple import barfeed # generalise the barfeed

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("strategyfile", help="strategy file", type=str)
    parser.add_argument("marketdatafile", help="market data file", type=str)
    args = parser.parse_args()
    strategyfile = args.strategyfile
    marketdatafile = args.marketdatafile
    logger.info('Strategy file %s' % (strategyfile))
    logger.info('Market data file %s' % (marketdatafile))
    return strategyfile,marketdatafile

def load_strategy(strategy_path):
    logger.info("Loading strategy source code from %s" % (strategy_path))
    strategy_module = imp.load_source('BacktestingStrategy',strategy_path)
    strategy_name, strategy_class = inspect.getmembers(strategy_module)[0] #loads first class found
    logger.info("Found strategy %s" % (strategy_name))
    return strategy_class

def load_market_data(marketdatafile):
    logger.info("Loading market data from %s" % (marketdatafile))
    feed = barfeed.Feed()  
    feed.addBarsFromCSV('instrument', marketdatafile)
    return feed

logger = pyalgotrade.logger.getLogger("performance")
logger.info("Starting")

strategyfile,marketdatafile = parse_arguments()
#strategyfile,marketdatafile = 'strategy/rsi2.py','marketdata/BTC.E2q_XRP-last-90-day-ripplecharts.csv'

strategy = load_strategy(strategyfile)
feed = load_market_data(marketdatafile)

# Evaluate the strategy with the feed's bars.
#myStrategy = sma_crossover.SMACrossOver(feed, "orcl", 20)
parameters = strategy.parameters()
mid_parameters = [sum(x) / len(x) for x in parameters]
logger.info("Mid-parameters found %s" % str(mid_parameters))
myStrategy = strategy(feed,'instrument', *mid_parameters)

# Attach different analyzers to a strategy before executing it.
retAnalyzer = returns.Returns()
sharpeRatioAnalyzer = sharpe.SharpeRatio()
drawDownAnalyzer = drawdown.DrawDown()
tradesAnalyzer = trades.Trades()

myStrategy.attachAnalyzer(retAnalyzer)
myStrategy.attachAnalyzer(sharpeRatioAnalyzer)
myStrategy.attachAnalyzer(drawDownAnalyzer)
myStrategy.attachAnalyzer(tradesAnalyzer)


# Run the strategy.
myStrategy.run()

# Results
# Strategy Risk = Probability x Impact
# = ( Profitable trades / total trades ) x Cumulative Returns
profitable_ratio = (float(tradesAnalyzer.getProfitableCount()) / float(tradesAnalyzer.getCount()))
riskscore = profitable_ratio * retAnalyzer.getCumulativeReturns()[-1] * 100

results = {
    'sharpe':       sharpeRatioAnalyzer.getSharpeRatio(0.05),
    'return':       retAnalyzer.getCumulativeReturns()[-1] * 100,
    'count':        tradesAnalyzer.getCount(),
    'profit_count': tradesAnalyzer.getProfitableCount(),
    'profitable_ratio': profitable_ratio,
    'riskscore': riskscore
    }

logger.info("Final portfolio value: $%.2f" % myStrategy.getResult())
logger.info("Cumulative returns: %.2f %%" % (retAnalyzer.getCumulativeReturns()[-1] * 100))
logger.info("Sharpe ratio: %.2f" % (sharpeRatioAnalyzer.getSharpeRatio(0.05)))
logger.info("Max. drawdown: %.2f %%" % (drawDownAnalyzer.getMaxDrawDown() * 100))
logger.info("Longest drawdown duration: %s" % (drawDownAnalyzer.getLongestDrawDownDuration()))

logger.info("Total trades: %d" % (tradesAnalyzer.getCount()))
if tradesAnalyzer.getCount() > 0:
    profits = tradesAnalyzer.getAll()
    logger.info("Avg. profit: $%2.f" % (profits.mean()))
    logger.info("Profits std. dev.: $%2.f" % (profits.std()))
    logger.info("Max. profit: $%2.f" % (profits.max()))
    logger.info("Min. profit: $%2.f" % (profits.min()))
    returns = tradesAnalyzer.getAllReturns()
    logger.info("Avg. return: %2.f %%" % (returns.mean() * 100))
    logger.info("Returns std. dev.: %2.f %%" % (returns.std() * 100))
    logger.info("Max. return: %2.f %%" % (returns.max() * 100))
    logger.info("Min. return: %2.f %%" % (returns.min() * 100))

logger.info("Profitable trades: %d" % (tradesAnalyzer.getProfitableCount()))
if tradesAnalyzer.getProfitableCount() > 0:
    profits = tradesAnalyzer.getProfits()
    logger.info("Avg. profit: $%2.f" % (profits.mean()))
    logger.info("Profits std. dev.: $%2.f" % (profits.std()))
    logger.info("Max. profit: $%2.f" % (profits.max()))
    logger.info("Min. profit: $%2.f" % (profits.min()))
    returns = tradesAnalyzer.getPositiveReturns()
    logger.info("Avg. return: %2.f %%" % (returns.mean() * 100))
    logger.info("Returns std. dev.: %2.f %%" % (returns.std() * 100))
    logger.info("Max. return: %2.f %%" % (returns.max() * 100))
    logger.info("Min. return: %2.f %%" % (returns.min() * 100))

logger.info("Unprofitable trades: %d" % (tradesAnalyzer.getUnprofitableCount()))
if tradesAnalyzer.getUnprofitableCount() > 0:
    losses = tradesAnalyzer.getLosses()
    logger.info("Avg. loss: $%2.f" % (losses.mean()))
    logger.info("Losses std. dev.: $%2.f" % (losses.std()))
    logger.info("Max. loss: $%2.f" % (losses.min()))
    logger.info("Min. loss: $%2.f" % (losses.max()))
    returns = tradesAnalyzer.getNegativeReturns()
    logger.info("Avg. return: %2.f %%" % (returns.mean() * 100))
    logger.info("Returns std. dev.: %2.f %%" % (returns.std() * 100))
    logger.info("Max. return: %2.f %%" % (returns.max() * 100))
    logger.info("Min. return: %2.f %%" % (returns.min() * 100))

logger.info("JSON Result: %s" % str(results))

logger.info("Finishing")